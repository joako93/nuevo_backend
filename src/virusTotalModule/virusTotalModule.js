const axios = require('axios');
var FormData = require('form-data');
const headers = { 'x-apikey': process.env.VIRUSTOTALTOKEN }

const getFileId = (file) => {
    let formData = new FormData();
    formData.append('file', file)

    formData = { file: JSON.stringify( file ) }

    axios.post('https://www.virustotal.com/api/v3/files', formData, { headers })
    .then((res) => {
        console.log('res: ', res.data.data.id)
        let fileId = res.data.data.id;
        console.log('fileId tiene: ', fileId)

        /*         axios.get(`https://www.virustotal.com/api/v3/analyses/${fileId}`, { headers }).then((response) => {
                    console.log('respuesta final tiene: ', response);
                    axios.post('localhost:4000/api/analyze', response)
                        .then(resultadoFinal => { console.log('lo que vuelve del nuevo back: ', resultadoFinal) })
                }).catch(err => {
                    console.log(err);
                }); */
    }).catch(err => {
        console.log(err);
    });
}

module.exports = { getFileId }