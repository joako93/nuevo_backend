const mongoose = require('mongoose')
const { Schema } = mongoose

const FileToAnalize = new Schema({
    "data": {
        attributes: {
            "date": Number,
            "stats": {
                "confirmed-timeout": Number,
                "failure": Number,
                "harmless": Number,
                "malicious": Number,
                "suspicious": Number,
                "timeout": Number,
                "type-unsupported": Number,
                "undetected": Number,
            },
            "status": String,
        },
        "id": String,
        "type": String
    },
    "meta": {
        "file_info": {
            "md5": String,
            "name": String,
            "sha1": String,
            "sha256": String,
            "size": Number
        }
    }
})

module.exports = mongoose.model('FileToAnalize', FileToAnalize)