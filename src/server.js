const express = require('express')
const morgan = require('morgan')
const mongoose = require('mongoose')
require('dotenv').config();


// Definitions
const app = express()
const dbURI = process.env.DBURI
const mongooseOptions = { useUnifiedTopology: true, useNewUrlParser: true, auto_reconnect: true }
const db = mongoose.connection;

// Database events TODO: Ver cuando conectar y desconectar de la bd
db.on('error', function (error) {
    mongoose.disconnect();
});
db.on('connected', function () {
    console.log('MongoDB connected!');
});
db.once('open', function () {
    console.log('MongoDB connection opened!');
});
db.on('reconnected', function () {
    console.log('MongoDB reconnected!');
});
db.on('disconnected', function () {
    console.log('MongoDB disconnected!');
    mongoose.connect(dbURI, mongooseOptions);
});

mongoose.connect(dbURI, mongooseOptions);

// Server setup
app.use(morgan('dev'))
app.use(express.json())
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, DELETE, OPTIONS"
    );
    next();
});

// Server routing
app.use('/api/analyze', require('./routes/analyze'))
app.use(express.static(__dirname + '/public'))

// Server start
app.listen(process.env.PORT || 4000);